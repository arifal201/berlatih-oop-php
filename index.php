<?php 
  require("animal.php");
  require("ape.php");
  require("frog.php");
  $hewan = new Animal("shaun");

  echo "Name: ".$hewan->name."<br>";
  echo "Legs: ". $hewan->legs."<br>";
  echo "Cold Blooded: ". $hewan->cold_blooded."<br>";
  echo "<br>";
  $kodok = new Frog("buduk");
  echo "Name: ".$kodok->name."<br>";
  echo "Legs: ".$kodok->legs."<br>";
  echo "Cold Blooded: ".$kodok->cold_blooded."<br>";
  echo "Jump: ";
  $kodok->jump();
  echo "<br><br>";
  $sungokong = new Ape("kera sakti");
  echo "Name: ".$sungokong->name."<br>";
  echo "Legs: ".$sungokong->legs."<br>";
  echo "Cold Blooded: ".$sungokong->cold_blooded."<br>";
  echo "Yell: ";
  $sungokong->yell();
  echo "<br> <br>";

?>