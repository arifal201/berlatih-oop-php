<?php 
  class Animal {
    public $legs = 2;
    public $cold_blooded = false;
    public function __construct($name){
      $this->name = $name;
      if($name == "snake"|| $name == "lizard"|| $name == "fish"){
        $this->cold_blooded = true;
      }
      if($this->cold_blooded == false){
        $this->cold_blooded = "no";
      }else{
        $this->cold_blooded = "yes";
      }
      if($name == "shaun"|| $name == "buduk"){
        $this->legs = 4;
      }
    }
  }
?>